package jogodoanagrama;
import java.util.ArrayList;
import java.util.Collections;


public class GeradorDeAnagrama {
    
    public String gerarAnagrama(String palavra){
        ArrayList<Character> listaDeCaracteres = new ArrayList<>();
        for(char caractere : palavra.toCharArray()){
            listaDeCaracteres.add(caractere);
        }
        Collections.shuffle(listaDeCaracteres);
        char[] arrayDeCaracteres = new char[listaDeCaracteres.size()];
        for(int i = 0; i < listaDeCaracteres.size(); i++){
            arrayDeCaracteres[i] = listaDeCaracteres.get(i);
        }
        String palavraEmbaralhada = new String(arrayDeCaracteres);
        return palavraEmbaralhada;
    }
    
}
