package jogodoanagrama;

import javax.swing.JOptionPane;

/**
 *
 * Jogo do anagrama
 *
 * O jogo começa com o jogador 1 inserindo uma palavra para ser embaralhada.
 *
 * Depois, o jogador 2 tentará acertar a palavra embaralhada em repetidas
 * tentativas.
 *
 * Cada tentativa de acerto (certa ou errada) deve ser contabilizada.
 *
 * Palavras com espaços não devem ser permitidas, mas quaisquer outros
 * caracteres são válidos.
 *
 */
public class JogoDoAnagrama {

    public static boolean acertou;

    public static void main(String[] args) {
        String palavraOriginal = JOptionPane.showInputDialog("Digite a palavra que deseja embaralhar");

        Validador validador = new Validador();
        try {
            while (!validador.validar(palavraOriginal)) {
                JOptionPane.showMessageDialog(null, "A palavra digitada não é válida.");
                palavraOriginal = JOptionPane.showInputDialog("Digite outra palavra que deseje embaralhar");
            }
        } catch (java.lang.NullPointerException e) {
            System.exit(0);
        }

        GeradorDeAnagrama gerador = new GeradorDeAnagrama();
        String palavraAnagrama = gerador.gerarAnagrama(palavraOriginal);

        acertou = false;
        int tentativas = 0;

        do {
            String tentativa = JOptionPane.showInputDialog("O anagrama é " + palavraAnagrama + "\n" + "Qual é a palavra original?");

            try {
                if (tentativa.equalsIgnoreCase(palavraOriginal)) {
                    acertou = true;
                } else {
                    acertou = false;
                    tentativas += 1;
                }
            } catch (java.lang.NullPointerException e) {
                tentativa = "";
            }

        } while (!acertou);

        JOptionPane.showMessageDialog(null, "Parabéns! Você acertou em " + tentativas + " tentativas.");
    }

}
